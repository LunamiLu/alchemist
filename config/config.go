package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"
)

type Config struct {
	InstallDir           string `json:"install_dir"`
	OutputLocation       string `json:"output"`
	PreviousDataLocation string `json:"previous_data"`

	DefaultGameIniLocation  string `json:"default_game_location"`
	DefaultSkinIniLocation  string `json:"default_skin_location"`
	BuildPropertiesLocation string `json:"build_properties_location"`
	LocalizationLocation    string `json:"localization_location"`

	Languages     map[string]string `json:"languages"`
	SkillUpgrades map[string]string `json:"skill_upgrades"`
	SkillButtons  map[string]string `json:"skill_buttons"`
	IsCore        bool              `json:"is_core"`
	Heroes        heroesConfig      `json:"heroes"`
}

type heroesConfig struct {
	ShortNames map[string]string `json:"short_names"`
}

func New(configLocation string) *Config {
	config := Config{}
	log.Printf("Initializing Config with config file: %s\n", configLocation)
	data, err := ioutil.ReadFile(configLocation)

	if err != nil {
		log.Fatal(err)
	}

	jsonErr := json.Unmarshal(data, &config)

	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	if strings.EqualFold(config.InstallDir, "") {
		log.Fatal("Gigantic Install Dir has not been set. Use the flag -install_dir to set the directory. For more information use -help")
	}

	config.InstallDir = strings.TrimSuffix(config.InstallDir, "/")
	config.LocalizationLocation = config.InstallDir + strings.TrimSuffix(config.LocalizationLocation, "/")
	config.BuildPropertiesLocation = config.InstallDir + config.BuildPropertiesLocation
	config.DefaultGameIniLocation = config.InstallDir + config.DefaultGameIniLocation
	config.DefaultSkinIniLocation = config.InstallDir + config.DefaultSkinIniLocation

	if strings.EqualFold(config.OutputLocation, "") {
		t := time.Now()
		timeString := fmt.Sprintf("%d-%02d-%02d_%02d-%02d-%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
		fmt.Println(timeString)
		if config.IsCore {
			config.OutputLocation = "giganticData-Core-" + timeString + ".json"
		} else {
			config.OutputLocation = "giganticData-Live-" + timeString + ".json"
		}
	}

	return &config
}

//ConfigChangeLocalization finds language in map to return shorter language code
func (config Config) ConfigChangeLocalization(language string) string {
	if _, ok := config.Languages[language]; ok {
		return config.Languages[language]
	}
	// else
	return language
}

//ConfigChangeSkillUpgrades findsd upgradeName in map to return shorter upgradeName
func (config Config) ConfigChangeSkillUpgrades(upgradeName string) string {
	if _, ok := config.SkillUpgrades[upgradeName]; ok {
		return config.SkillUpgrades[upgradeName]
	}
	// else
	return upgradeName
}

func (config Config) ConfigChangeHeroShortName(name string) string {
	if _, ok := config.Heroes.ShortNames[name]; ok {
		return config.Heroes.ShortNames[name]
	}
	// else
	return name
}

func (config Config) ChangeHeroShortNameInString(text string) string {
	for key, value := range config.Heroes.ShortNames {
		text = strings.Replace(text, key, value, -1)
	}
	return text
}

func (config Config) ConfigChangeSkillButton(button string) string {
	if _, ok := config.SkillButtons[button]; ok {
		return config.SkillButtons[button]
	}
	// else
	return button
}
