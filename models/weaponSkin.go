package models

import (
	"gitlab.com/ggunleashed/alchemist/config"
	"gitlab.com/ggunleashed/machine"
	"strings"
)

type WeaponSkin struct {
	IniTitle              string `json:"-"`
	IniClass              string `json:"-"`
	ResourceID            string `json:"-"`
	UIResourceID          string `json:"-"`
	HeroName              string `json:"-"`
	HeroArchetypeName     string `json:"-"`
	GatingType            string `json:"-"`
	ProviderPawnClassPath string `json:"-"`
	SkinAssetPath         string `json:"-"`
	ColourVariantID       string `json:"colourVariant"`

	// Localization
	SkinDisplayName map[string]string `json:"name"`
	SkinDescription map[string]string `json:"description"`
}

func NewWeaponSkin(iniData machine.IniData, config config.Config) (weaponSkin WeaponSkin) {
	weaponSkin.IniTitle = iniData.Title
	weaponSkin.IniClass = iniData.Class
	weaponSkin.ResourceID = iniData.Data["ResourceID"]
	weaponSkin.UIResourceID = iniData.Data["UIResourceID"]
	weaponSkin.HeroName = strings.TrimSuffix(iniData.Data["HeroArchetypeName"], "_Pawn_Arch")
	weaponSkin.HeroArchetypeName = iniData.Data["HeroArchetypeName"]
	weaponSkin.GatingType = iniData.Data["GatingType"]
	weaponSkin.ProviderPawnClassPath = iniData.Data["ProviderPawnClassPath"]
	weaponSkin.SkinAssetPath = iniData.Data["SkinAssetPath"]

	splitResourceID := strings.Split(weaponSkin.ResourceID, "_")
	weaponSkin.ColourVariantID = splitResourceID[len(splitResourceID)-1]

	// Initialize localization variables
	weaponSkin.SkinDisplayName = make(map[string]string)
	weaponSkin.SkinDescription = make(map[string]string)

	return weaponSkin
}

func (weaponSkin *WeaponSkin) AddLocalization(language string, iniData machine.IniData, config config.Config) {
	weaponSkin.SkinDisplayName[language] = iniData.Data["SkinDisplayName"]
	weaponSkin.SkinDescription[language] = iniData.Data["SkinDescription"]
}
