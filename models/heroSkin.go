package models

import (
	"gitlab.com/ggunleashed/alchemist/config"
	"gitlab.com/ggunleashed/machine"
	"strings"
)

type HeroSkin struct {
	IniTitle              string `json:"-"`
	IniClass              string `json:"-"`
	ResourceID            string `json:"-"`
	UIResourceID          string `json:"-"`
	HeroName              string `json:"-"`
	HeroArchetypeName     string `json:"-"`
	HeroSkinType          string `json:"-"`
	GatingType            string `json:"-"`
	ProviderPawnClassPath string `json:"-"`
	BaseHeroSkinID        string `json:"-"`
	SkinAssetPath         string `json:"-"`
	ColourVariantID       string `json:"colourVariant"`

	// Localization
	SkinDisplayName map[string]string `json:"name"`
	SkinDescription map[string]string `json:"description"`
}

func NewHeroSkin(iniData machine.IniData, config config.Config) (heroSkin HeroSkin) {
	heroSkin.IniTitle = iniData.Title
	heroSkin.IniClass = iniData.Class
	heroSkin.ResourceID = iniData.Data["ResourceID"]
	heroSkin.UIResourceID = iniData.Data["UIResourceID"]
	heroSkin.HeroArchetypeName = iniData.Data["HeroArchetypeName"]
	heroSkin.HeroSkinType = iniData.Data["HeroSkinType"]
	heroSkin.HeroName = iniData.Data["HeroName"]
	heroSkin.GatingType = iniData.Data["GatingType"]
	heroSkin.ProviderPawnClassPath = iniData.Data["ProviderPawnClassPath"]
	heroSkin.SkinAssetPath = iniData.Data["SkinAssetPath"]
	heroSkin.BaseHeroSkinID = iniData.Data["BaseHeroSkinID"]

	splitResourceID := strings.Split(heroSkin.ResourceID, "_")
	heroSkin.ColourVariantID = splitResourceID[len(splitResourceID)-1]

	// Initialize localization variables
	heroSkin.SkinDisplayName = make(map[string]string)
	heroSkin.SkinDescription = make(map[string]string)

	return heroSkin
}

func (heroSkin *HeroSkin) AddLocalization(language string, iniData machine.IniData, config config.Config) {
	heroSkin.SkinDisplayName[language] = iniData.Data["SkinDisplayName"]
	heroSkin.SkinDescription[language] = iniData.Data["SkinDescription"]
}
