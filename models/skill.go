package models

import (
	"strings"

	"gitlab.com/ggunleashed/alchemist/config"
	"gitlab.com/ggunleashed/machine"
)

//Skill struct
type Skill struct {
	//Localization
	SkillDisplayName map[string]string `json:"name"`
	SkillDescription map[string]string `json:"description"`
	SkillLore        map[string]string `json:"lore"`

	IniTitle          string `json:"-"`
	SkillName         string `json:"-"`
	ResourceID        string `json:"-"`
	UIResourceID      string `json:"-"`
	HeroArchetypeName string `json:"-"`

	//Skill Upgrades
	SkillUpgrades map[string]SkillUpgrade `json:"upgrades"`
}

//NewSkill creates a new Skill
func NewSkill(iniData machine.IniData, config config.Config) (skill Skill) {
	skill.IniTitle = iniData.Title
	skill.SkillName = iniData.Data["SkillName"]
	skill.ResourceID = strings.TrimSuffix(iniData.Title, "_Provider")
	skill.UIResourceID = iniData.Data["UIResourceID"]
	skill.HeroArchetypeName = iniData.Data["HeroArchetypeName"]

	//Making map for SkillUpgrades
	skill.SkillUpgrades = make(map[string]SkillUpgrade)

	//Making the maps for the localizations
	skill.SkillDisplayName = make(map[string]string)
	skill.SkillDescription = make(map[string]string)
	skill.SkillLore = make(map[string]string)

	return skill
}

func (skill *Skill) AddLocalization(language string, iniData machine.IniData, config config.Config) {
	skill.SkillDisplayName[language] = iniData.Data["SkillDisplayName"]
	skill.SkillDescription[language] = iniData.Data["SkillDescription"]

	skill.SkillDescription[language] = strings.Replace(skill.SkillDescription[language], "`skill1button", config.ConfigChangeSkillButton("`skill1button"), -1)
	skill.SkillDescription[language] = strings.Replace(skill.SkillDescription[language], "`skill2button", config.ConfigChangeSkillButton("`skill2button"), -1)
	skill.SkillDescription[language] = strings.Replace(skill.SkillDescription[language], "`skill3button", config.ConfigChangeSkillButton("`skill3button"), -1)
	skill.SkillDescription[language] = strings.Replace(skill.SkillDescription[language], "`skill4button", config.ConfigChangeSkillButton("`skill4button"), -1)
	skill.SkillDescription[language] = strings.Replace(skill.SkillDescription[language], "`focusskillbutton", config.ConfigChangeSkillButton("`focusskillbutton"), -1)
}

func (skill *Skill) AddSkillUpgrade(skillUpgrade SkillUpgrade, config config.Config) {
	skill.SkillUpgrades[config.ConfigChangeSkillUpgrades(skillUpgrade.UpgradeTier)] = skillUpgrade
}
