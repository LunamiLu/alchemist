package models

import (
	"gitlab.com/ggunleashed/alchemist/config"
	"strings"
)

type Skin struct {
	ID                string `json:"id"`
	HeroName          string `json:"-"`
	HeroArchetypeName string `json:"-"`
	GatingType        string `json:"-"`

	ColourVariantsHeroSkin   []HeroSkin   `json:"heroSkins"`
	ColourVariantsWeaponSkin []WeaponSkin `json:"weaponSkins"`
	IsPrestige               bool         `json:"isPrestige"`
}

func NewSkinFromHeroSkin(heroSkin HeroSkin, config config.Config) (skin Skin) {
	skin.ID = config.ChangeHeroShortNameInString(strings.TrimPrefix(heroSkin.ResourceID[:strings.LastIndex(heroSkin.ResourceID, "_")], "HeroSkin_"))
	skin.HeroName = heroSkin.HeroName
	skin.HeroArchetypeName = heroSkin.HeroArchetypeName
	skin.GatingType = heroSkin.GatingType

	skin.ColourVariantsHeroSkin = append(skin.ColourVariantsHeroSkin, heroSkin)

	if strings.EqualFold(heroSkin.GatingType, "ESGT_Prestige") {
		skin.IsPrestige = true
	}
	return skin
}

func NewSkinFromWeaponSkin(weaponSkin WeaponSkin, config config.Config) (skin Skin) {
	skin.ID = config.ChangeHeroShortNameInString(strings.TrimPrefix(weaponSkin.ResourceID[:strings.LastIndex(weaponSkin.ResourceID, "_")], "HeroSkin_"))
	skin.HeroName = weaponSkin.HeroName
	skin.HeroArchetypeName = weaponSkin.HeroArchetypeName
	skin.GatingType = weaponSkin.GatingType

	skin.ColourVariantsWeaponSkin = append(skin.ColourVariantsWeaponSkin, weaponSkin)
	if strings.EqualFold(weaponSkin.GatingType, "ESGT_Prestige") {
		skin.IsPrestige = true
	}
	return skin
}
